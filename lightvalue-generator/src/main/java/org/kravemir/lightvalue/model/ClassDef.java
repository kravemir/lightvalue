package org.kravemir.lightvalue.model;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ClassDef {
    private String name;

    private Map<String, ClassDef> subClasses = new LinkedHashMap<>();
    private Map<String, PropertyDef> properties = new LinkedHashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<ClassDef> getSubClassDefs() {
        return subClasses.values();
    }

    public Collection<PropertyDef> getProperties() {
        return properties.values();
    }

    public void addSubClass(ClassDef classDef) {
        if(subClasses.get(classDef.getName()) != null) {
            throw new RuntimeException(String.format("Class %s already exists", classDef.getName()));
        }

        subClasses.put(classDef.getName(), classDef);
    }

    public void addProperty(PropertyDef propertyDef) {
        if(properties.get(propertyDef.getName()) != null) {
            throw new RuntimeException(String.format("Property %s already exists", propertyDef.getName()));
        }

        properties.put(propertyDef.getName(), propertyDef);
    }

    @Override
    public String toString() {
        return "ClassDef{" +
                "name='" + name + '\'' +
                ", subClasses=" + subClasses +
                ", properties=" + properties +
                '}';
    }
}
