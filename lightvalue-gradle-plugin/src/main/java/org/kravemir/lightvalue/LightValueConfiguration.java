package org.kravemir.lightvalue;

import org.gradle.api.tasks.SourceSet;

import java.io.File;

public class LightValueConfiguration {
    
    private final String name;
    private final LightValueGeneratorTask task;

    public LightValueConfiguration(String name, LightValueGeneratorTask task) {
        this.name = name;
        this.task = task;
    }

    public String getName() {
        return name;
    }

    public LightValueGeneratorTask getTask() {
        return task;
    }

    public boolean getGenerateJacksonMixIns() {
        return task.getGenerateJacksonMixIns();
    }

    public void setGenerateJacksonMixIns(boolean generateJacksonMixIns) {
        task.setGenerateJacksonMixIns(generateJacksonMixIns);
    }

    public void registerInSourceSets(SourceSet... sourceSets) {
        task.registerInSourceSets(sourceSets);
    }

    public void source(File srcDir) {
        task.source(srcDir);
    }

    public void setOutputDir(File outputDir) {
        task.setOutputDir(outputDir);
    }
}
