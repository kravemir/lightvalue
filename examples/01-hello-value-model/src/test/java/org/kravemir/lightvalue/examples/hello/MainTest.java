package org.kravemir.lightvalue.examples.hello;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.Assert.assertThat;

public class MainTest {

    private static final String[] expectedOutput = new String[] {
            "============================================================",
            "From: LightValue Example01 <something@somewhere.in.space>",
            "To:   LightValue User <someone@earth.planet>",
            "============================================================",
            "Hello LightValue User!",
            "",
            "Best wishes,",
            "LightValue Example01",
            "============================================================"
    };

    @Test
    public void testMain() {
        final PrintStream originalOut = System.out;

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            System.setOut(new PrintStream(outputStream));
            Main.main(new String[]{});

            String output = new String(outputStream.toByteArray());
            assertThat(output.split("\n"), arrayContaining(expectedOutput));
        } finally {
            System.setOut(originalOut);

        }
    }
}
