package org.kravemir.lightvalue.buildsrc;

import jflex.Main;
import jflex.SilentExit;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.SourceTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GenerateLexerTask extends SourceTask {
    private File outputDir;

    @TaskAction
    public void generate() {
        List<String> args = new ArrayList<>();
        args.add("-d");
        args.add(getOutputDir().getPath());
        args.add("-q");
        args.addAll(getSource().getFiles().stream().map(File::toString).collect(Collectors.toList()));

        try {
            Main.generate(args.toArray(new String[0]));
        } catch (SilentExit e) {
            throw new RuntimeException("JFlex generation failed", e);
        }
    }

    @OutputDirectory
    public File getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(final File outputDir) {
        this.outputDir = outputDir;
    }
}
